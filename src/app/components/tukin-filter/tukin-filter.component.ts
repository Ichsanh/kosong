import { Component, Input, OnInit } from '@angular/core';
import { ToastService } from 'src/app/services/toast.service';
import { TukinService } from './../../services/tukin.service';


@Component({
  selector: 'app-tukin-filter',
  templateUrl: './tukin-filter.component.html',
  styleUrls: ['./tukin-filter.component.scss'],
})
export class TukinFilterComponent implements OnInit {
  @Input() loginUser: any;
  public postData = {
    tahun: '',
    bulan: '',
    nip_pegawai_lama: '',
    
  };
  
  constructor(
    private tukinService: TukinService,
    private toastService: ToastService) {}

  ngOnInit() {}

  tukinUpdateAction() {
    if (this.postData.tahun.length > 0) {
      
      this.postData.nip_pegawai_lama = this.loginUser.nip_pegawai_lama;
      
      this.tukinService.tukinData(this.postData).subscribe((res: any) => {
        // this.postData.feed = '';
        
        
          if (res.tukinData) {
            this.tukinService.updateTukinData(res.tukinData);
            console.log(res.tukinData);
          } else {
            this.toastService.presentToast('Data Tidak Tersedia');
          }
          
        

      });
    }
  }
}

