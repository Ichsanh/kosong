import { Component, Input, OnInit } from '@angular/core';
import { ToastService } from 'src/app/services/toast.service';
import { GajiService } from './../../services/gaji.service';


@Component({
  selector: 'app-gaji-filter',
  templateUrl: './gaji-filter.component.html',
  styleUrls: ['./gaji-filter.component.scss'],
})
export class GajiFilterComponent implements OnInit {
  @Input() loginUser: any;
  public postData = {
    tahun: '',
    bulan: '',
    nip_pegawai_lama: '',
    
  };
  constructor(private gajiService: GajiService, private toastService: ToastService) {}

  ngOnInit() {}

  gajiUpdateAction() {
    if (this.postData.tahun.length > 0) {
      
      this.postData.nip_pegawai_lama = this.loginUser.nip_pegawai_lama;
      
      this.gajiService.gajiData(this.postData).subscribe((res: any) => {
        // this.postData.feed = '';
        
        if (res.gajiData) {
          this.gajiService.updateGajiData(res.gajiData);
        console.log(res.gajiData);
        } else {
          this.toastService.presentToast('Data Tidak Tersedia');
        }
      });
    }
  }
}

