import { Component, Input, OnInit } from '@angular/core';
import { AlertService } from './../../services/alert.service';
import { GajiService } from './../../services/gaji.service';

@Component({
  selector: 'app-gaji-card',
  templateUrl: './gaji-card.component.html',
  styleUrls: ['./gaji-card.component.scss'],
})

export class GajiCardComponent implements OnInit {
  @Input() loginUser: any;
  gajiData: any;
  postData = {
    nip_pegawai_lama: '',
    
  };

  constructor(
    private gajiSerivce: GajiService,
    // private alertSerivce: AlertService
  ) {}

  ngOnInit() {
    this.gajiSerivce.gajiData$.subscribe((res: any) => {
      this.gajiData = res;
      
      // console.log(this.gajiData);
    });
  }

}
