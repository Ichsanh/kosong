import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GajiCardComponent } from './gaji-card.component';

describe('GajiCardComponent', () => {
  let component: GajiCardComponent;
  let fixture: ComponentFixture<GajiCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GajiCardComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GajiCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
