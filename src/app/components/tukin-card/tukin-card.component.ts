import { Component, Input, OnInit } from '@angular/core';
// import { AlertService } from './../../services/alert.service';
import { TukinService } from './../../services/tukin.service';

@Component({
  selector: 'app-tukin-card',
  templateUrl: './tukin-card.component.html',
  styleUrls: ['./tukin-card.component.scss'],
})

export class TukinCardComponent implements OnInit {
  @Input() loginUser: any;
  tukinData: any;
  postData = {
    nip_pegawai_lama: '',
    
  };

  constructor(
    private tukinSerivce: TukinService,
    // private alertSerivce: AlertService
  ) {}

  ngOnInit() {
    this.tukinSerivce.tukinData$.subscribe((res: any) => {
      this.tukinData = res;
      
      console.log(this.tukinData);
    });
  }

}
