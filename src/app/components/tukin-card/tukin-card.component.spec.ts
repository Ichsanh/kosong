import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TukinCardComponent } from './tukin-card.component';

describe('TukinCardComponent', () => {
  let component: TukinCardComponent;
  let fixture: ComponentFixture<TukinCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TukinCardComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TukinCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
