import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TimeAgoPipe } from 'time-ago-pipe';
import { FeedCardComponent } from './feed-card/feed-card.component';
import { FeedUpdateComponent } from './feed-update/feed-update.component';
import { GajiCardComponent } from './gaji-card/gaji-card.component';
import { GajiFilterComponent } from './gaji-filter/gaji-filter.component';
import { SlidesComponent } from './slides/slides.component';
import { StartButtonComponent } from './start-button/start-button.component';
import { TimeagoComponent } from './timeago/timeago.component';
import { TukinCardComponent } from './tukin-card/tukin-card.component';
import { TukinFilterComponent } from './tukin-filter/tukin-filter.component';
@NgModule({
  declarations: [
    SlidesComponent,
    StartButtonComponent,
    FeedCardComponent,
    FeedUpdateComponent,
    GajiCardComponent,
    GajiFilterComponent,
    TukinCardComponent,
    TukinFilterComponent,
    TimeagoComponent,
    TimeAgoPipe
  ],
  exports: [
    SlidesComponent,
    StartButtonComponent,
    FeedCardComponent,
    FeedUpdateComponent,
    GajiCardComponent,
    GajiFilterComponent,
    TukinCardComponent,
    TukinFilterComponent,
    TimeagoComponent
  ],
  imports: [CommonModule, FormsModule, IonicModule]
})
export class ComponentsModule {}
