import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {
  passwordData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService) {}

  changePasswordData(data: any) {
    this.passwordData$.next(data);
  }

  getCurrentPasswordData() {
    return this.passwordData$.getValue();
  }

  updatePasswordData(newpassword: any) {
    let data = [];
    data.push(newpassword);
    // let currentpasswordData = this.getCurrentPasswordData();
    // let newpasswordUpdateData = data.concat(currentpasswordData);
    // this.changepasswordData(newpasswordUpdateData);
    this.changePasswordData(newpassword);
  }


  deletePasswordData(msgIndex: number) {
    let data = [];
    let currentpasswordData = this.getCurrentPasswordData();
    currentpasswordData.splice(msgIndex, 1);
    let newpasswordUpdateData = data.concat(currentpasswordData);
    this.changePasswordData(newpasswordUpdateData);
  }

  passwordData(postData: any): Observable<any> {
    return this.httpService.put('password', postData);
  }

  passwordDelete(postData: any): Observable<any> {
    return this.httpService.post('passwordDelete', postData);
  }

  passwordUpdate(postData: any): Observable<any> {
    return this.httpService.post('passwordUpdate', postData);
  }
}
