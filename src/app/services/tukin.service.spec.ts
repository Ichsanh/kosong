import { TestBed } from '@angular/core/testing';

import { TukinService } from './tukin.service';

describe('TukinService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TukinService = TestBed.get(TukinService);
    expect(service).toBeTruthy();
  });
});
