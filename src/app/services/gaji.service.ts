import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class GajiService {
  gajiData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService) {}

  changeGajiData(data: any) {
    this.gajiData$.next(data);
  }

  getCurrentGajiData() {
    return this.gajiData$.getValue();
  }

  updateGajiData(newGaji: any) {
    let data = [];
    data.push(newGaji);
    let currentGajiData = this.getCurrentGajiData();
    // let newGajiUpdateData = data.concat(currentGajiData);
    // this.changeGajiData(newGajiUpdateData);
    this.changeGajiData(newGaji);
  }


  deleteGajiData(msgIndex: number) {
    let data = [];
    let currentGajiData = this.getCurrentGajiData();
    currentGajiData.splice(msgIndex, 1);
    let newGajiUpdateData = data.concat(currentGajiData);
    this.changeGajiData(newGajiUpdateData);
  }

  gajiData(postData: any): Observable<any> {
    return this.httpService.post('gaji', postData);
  }

  gajiDelete(postData: any): Observable<any> {
    return this.httpService.post('gajiDelete', postData);
  }

  gajiUpdate(postData: any): Observable<any> {
    return this.httpService.post('gajiUpdate', postData);
  }
}
