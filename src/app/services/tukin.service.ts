import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TukinService {
  tukinData$ = new BehaviorSubject<any>([]);
  constructor(private httpService: HttpService) {}

  changeTukinData(data: any) {
    this.tukinData$.next(data);
  }

  getCurrentTukinData() {
    return this.tukinData$.getValue();
  }

  updateTukinData(newTukin: any) {
    let data = [];
    data.push(newTukin);
    let currenttukinData = this.getCurrentTukinData();
    // let newtukinUpdateData = data.concat(currenttukinData);
    // this.changetukinData(newtukinUpdateData);
    this.changeTukinData(newTukin);
  }


  deleteTukinData(msgIndex: number) {
    let data = [];
    let currentTukinData = this.getCurrentTukinData();
    currentTukinData.splice(msgIndex, 1);
    let newtukinUpdateData = data.concat(currentTukinData);
    this.changeTukinData(newtukinUpdateData);
  }

  tukinData(postData: any): Observable<any> {
    return this.httpService.post('tukin', postData);
  }

  tukinDelete(postData: any): Observable<any> {
    return this.httpService.post('tukinDelete', postData);
  }

  tukinUpdate(postData: any): Observable<any> {
    return this.httpService.post('tukinUpdate', postData);
  }
}
