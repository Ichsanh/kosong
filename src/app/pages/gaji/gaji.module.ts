import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from './../../components/components.module';
import { IonicModule } from '@ionic/angular';

import { GajiPage } from './gaji.page';

const routes: Routes = [
  {
    path: '',
    component: GajiPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GajiPage]
})
export class GajiPageModule {}
