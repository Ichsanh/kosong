import { Component, OnInit } from '@angular/core';
import { GajiService } from 'src/app/services/gaji.service';
import { AuthService } from './../../services/auth.service';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-gaji',
  templateUrl: './gaji.page.html',
  styleUrls: ['./gaji.page.scss']
})
export class GajiPage implements OnInit {
  public authUser: any;

  postData = {
    nip_pegawai_lama: '',
    // token: ''
  };
  constructor(
    private auth: AuthService,
    private gajiSerive: GajiService,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    
    this.auth.userData$.subscribe((res: any) => {
      this.authUser = res;
      this.gajiData();
    });
  }

  gajiData() {
    console.log(this.authUser);
    
    this.postData.nip_pegawai_lama = this.authUser.nip_pegawai_lama;
    // this.postData.token = this.authUser.token;
    if (this.postData.nip_pegawai_lama ) {
      this.gajiSerive.gajiData(this.postData).subscribe(
        (res: any) => {
          if (res.gajiData) {
            this.gajiSerive.changeGajiData(res.gajiData);
          } else {
            this.toastService.presentToast('Data Tidak Tersedia');
          }
          
        },
        (error: any) => {
          this.toastService.presentToast('Network Issue.');
        }
      );
    }
  }
}
