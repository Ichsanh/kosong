import { Component, OnInit } from '@angular/core';
import { FeedService } from 'src/app/services/feed.service';
import { AuthService } from './../../services/auth.service';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss']
})
export class FeedPage implements OnInit {
  public authUser: any;
  feedData: any;

  postData = {
    nip_pegawai_lama: ''
  };
  constructor(
    private auth: AuthService,
    private feedSerive: FeedService,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    this.auth.userData$.subscribe((res: any) => {
      this.authUser = res;
      this.feedData2();
      
    });
    this.feedSerive.feedData$.subscribe((res: any) => {
      this.feedData = res;
      console.log(this.feedData);
    });
    
  }

  feedData2() {
    // console.log(this.authUser);
    this.postData.nip_pegawai_lama = this.authUser.nip_pegawai_lama;
    
    if (this.postData.nip_pegawai_lama ) {
      this.feedSerive.feedData(this.postData).subscribe(
        (res: any) => {
          this.feedSerive.changeFeedData(res.feedData);
        },
        (error: any) => {
          this.toastService.presentToast('Network Issue.');
        }
      );
    }
  }
}
