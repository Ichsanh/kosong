import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-ganti-password',
  templateUrl: './ganti-password.page.html',
  styleUrls: ['./ganti-password.page.scss'],
})
export class GantiPasswordPage implements OnInit {
  postData = {
    
    password: ''
  };
  
  public authUser: any;
  constructor(
    private auth: AuthService,
  ) { }

  ngOnInit() {
    this.auth.userData$.subscribe((res: any) => {
      this.authUser = res;
      console.log("ganti_password2");
      console.log(this.authUser);
    });
  }

  

}
