import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TukinPage } from './tukin.page';

describe('TukinPage', () => {
  let component: TukinPage;
  let fixture: ComponentFixture<TukinPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TukinPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TukinPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
