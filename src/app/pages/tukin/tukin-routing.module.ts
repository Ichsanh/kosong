import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TukinPage } from './tukin.page';

const routes: Routes = [
  {
    path: '',
    component: TukinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TukinPageRoutingModule {}
