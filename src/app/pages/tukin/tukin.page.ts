import { Component, OnInit } from '@angular/core';
import { TukinService } from 'src/app/services/tukin.service';
import { AuthService } from './../../services/auth.service';
import { ToastService } from './../../services/toast.service';

@Component({
  selector: 'app-tukin',
  templateUrl: './tukin.page.html',
  styleUrls: ['./tukin.page.scss']
})
export class TukinPage implements OnInit {
  public authUser: any;

  postData = {
    nip_pegawai_lama: '',
    // token: ''
  };
  constructor(
    private auth: AuthService,
    private tukinSerive: TukinService,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    
    this.auth.userData$.subscribe((res: any) => {
      this.authUser = res;
      this.tukinData();
    });
  }

  tukinData() {
    
    this.postData.nip_pegawai_lama = this.authUser.nip_pegawai_lama;
    // this.postData.token = this.authUser.token;
    if (this.postData.nip_pegawai_lama ) {
      this.tukinSerive.tukinData(this.postData).subscribe(
        (res: any) => {
          this.tukinSerive.changeTukinData(res.tukinData);
        },
        (error: any) => {
          this.toastService.presentToast('Network Issue.');
        }
      );
    }
  }
}
