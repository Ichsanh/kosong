import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TukinPageRoutingModule } from './tukin-routing.module';

import { TukinPage } from './tukin.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TukinPageRoutingModule
  ],
  declarations: [TukinPage]
})
export class TukinPageModule {}
