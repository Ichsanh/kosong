import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { PasswordService } from 'src/app/services/password.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage implements OnInit {
  postData = {
    nip_pegawai_lama:"",
    password: ''
  };
  public authUser: any;
  constructor(private auth: AuthService, private toastService: ToastService, private passwordService: PasswordService) { }

  ngOnInit() {
    this.auth.userData$.subscribe((res: any) => {
      this.authUser = res;
      console.log(this.authUser);
    });
  }

  updatePassswordAction() {
    if (this.postData.password.length > 0) {
      
      this.postData.nip_pegawai_lama = this.authUser.nip_pegawai_lama;
      
      this.passwordService.passwordData(this.postData).subscribe((res: any) => {
        // this.postData.feed = '';
        
        if (res.passwordData) {
          this.passwordService.updatePasswordData(res.passwordData);
        console.log(res.passwordData);
        this.toastService.presentToast('Password telah diupdate');
        } else {
          this.toastService.presentToast('Data Tidak Tersedia');
        }
      });
    }
  }

}
