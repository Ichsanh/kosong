import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Tukin2PageRoutingModule } from './tukin2-routing.module';

import { Tukin2Page } from './tukin2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Tukin2PageRoutingModule
  ],
  declarations: [Tukin2Page]
})
export class Tukin2PageModule {}
