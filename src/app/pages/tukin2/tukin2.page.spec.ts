import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Tukin2Page } from './tukin2.page';

describe('Tukin2Page', () => {
  let component: Tukin2Page;
  let fixture: ComponentFixture<Tukin2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Tukin2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Tukin2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
