import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Tukin2Page } from './tukin2.page';

const routes: Routes = [
  {
    path: '',
    component: Tukin2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Tukin2PageRoutingModule {}
